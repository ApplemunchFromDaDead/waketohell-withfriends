/*
GAME.H
This file defines SPECIFICALLY the game logic, completely separate from any libraries.
Drawing and handling of sprites happens in the main_[platform].c file.
*/

// these define the amount of frames in animations
#define PLR_IDLEFRAMES 1
#define PLR_IDLE1FRAMES 2
// same for the entering door sprite
#define PLR_WALKFRAMES 4
#define PLR_SLEEPFRAMES 0

#define INTERTYPE_TRIGGER 0
#define INTERTYPE_DOOR 1
#define INTERTYPE_ARTIFACT 2
#define INTERTYPE_MOVETRACK 3
#define INTERTYPE_COAT 4
#define INTERTYPE_MOVEBLOCK 5
#define INTERTYPE_DECOR 254
#define INTERTYPE_YOU 255

#define ARTIFACT_BADGE 0 // lime badge
#define ARTIFACT_MIRROR 1 // hand mirror 
#define ARTIFACT_DONUT 2// mysterious doughnuts
#define ARTIFACT_KNIFE 3 // blood-stained knife

// according to Suckless, floats are overrated lmao
#define SUBPIXELUNIT_ACCURACY 32
// floats can be done, it just needs adaptation

// PRECALCULATIONS!!!
#define PRE_GRAVITY SUBPIXELUNIT_ACCURACY >> 2 // precalculated gravity, also doubles as player movement speed

#define FLAG_GROUNDED 1
#define FLAG_CANTMOVE 1 << 1 // disables player control, if any
#define FLAG_NOPHYS 1 << 2 // universal; if set, object refuses to move at all
#define FLAG_FLIPPED 1 << 3
#define FLAG_STEPDEBOUNCE 1 << 4 // used to see if the player already did a footstep
#define FLAG_HASCOAT 1 << 5 // player flag: does the player have their coat on?

#define PFLAG_HALTANIM 1 << 6 // if set, animations don't loop
#define PFLAG_REVERSEANIM 1 << 7 // if set, animations deliberately play in reverse

#define PSTATE_NORMAL 0
#define PSTATE_SLEEPING 1
#define PSTATE_CUTSCENE 2

// interactible flags
#define INTER_ACTIVE 1 << 7 // thing flag; is this thing NOT "in the room"?

#define KEY_UP 1
#define KEY_DOWN 1 << 1
#define KEY_LEFT 1 << 2
#define KEY_RIGHT 1 << 3
#define KEY_JUMP 1 << 4
#define KEY_ATTACK 1 << 5
#define KEY_MENU 1 << 6

// game states
#define GSTATE_MENU 0
#define GSTATE_PLAYING 1
#define GSTATE_BLANKSCREEN 2
#define GSTATE_PAUSED 3

#define GROUNDLEVEL 200

#define WIDTH 480
#define HEIGHT 270

#define WTHOPTS_NOFADE 1 // disables fading in/out for performance reasons
#define WTHOPTS_DEVCAM 1 << 1 // a toggle for the free camera
#define WTHOPTS_CENSOR 1 << 2 // are there fetuses playing my game?
#define WTHOPTS_DOSONG 1 << 5 // play music?
#define WTHOPTS_2XSIZE 1 << 6 // 2x the res?
#define WTHOPTS_FULLSC 1 << 7 // fullscreen enabled?

#define ACHIEVE_BEATME 1
#define ACHIEVE_NOCOAT 1 << 1
#define ACHIEVE_RISQUE 1 << 2
#define ACHIEVE_MYBLUD 1 << 3
#define ACHIEVE_NOTIME 1 << 4

// draw calls
#define DRAW_PLRUPARROW 1

#define DRAW_FADEIN 16 // begin fading in from black
#define DRAW_FADEOUT 17 // begin fading into black
#define DRAW_HALTFADE 18 // clear the screen of fading entirely

#define DRAW_CHECK2XSIZE 64
#define DRAW_CHECKFULLSC 65

#define DRAW_UPDATEMENU 127

#define MISC_PAUSEMUSIC 0
#define MISC_RESUMEMUSIC 1
#define MISC_STOPMUSIC 2

#define FAMILY_FRIENDLY 0

#define INTER_LIMIT 64 // how many interactibles can be on a level at a time?

#if FAMILY_FRIENDLY == 1
	#define WINTITLE "Wake to Chaos"
#else
	#define WINTITLE "Wake to Hell"
#endif

#define VERSION_NUMBER "with Friends! 0.1.3a"

#define WTH_keyPressed(keyvalue) (input_keys & keyvalue) && !(input_prevkeys & keyvalue)
// easy way to check if a key was down but NOT held, saves space for comment rambling

void signalDraw(uint8_t signal); // interface with main for drawing specific things at specific times
static inline void signalPlaySFX(uint8_t signal); // same stuff, but for sound effects
static inline void signalPlayMUS(uint8_t signal); // ditto, but for music
void signalMisc(uint8_t signal); // ditto, but for anything else

uint8_t GAME_STATE = 0;

uint8_t srvdata[2];

typedef struct {
	int x, y, hsp, vsp;
	int hsp_sub, vsp_sub;
	uint8_t flags, jumptimer, artifacts;
	uint8_t state;
	uint8_t animframe, animtimer, animflimit, animindex;
} WTH_Player;

typedef struct {
	uint8_t menuselect;
	uint8_t menuindex:4,
			menulimit:4;
} WTH_MainMenu;
// menuindex and menulimit are 4-bit variables (up to 15)

typedef struct {
	int x, y;
} WTH_Camera;

typedef struct {
	int x, y;
	uint16_t vars[8];
	uint8_t objID, flags;
} WTH_Interactible;

WTH_Player plr; // the one and only... uh... unstable furry man.
WTH_Camera cam; // and also the cameraman
WTH_Interactible interacts[INTER_LIMIT];
uint8_t interacts_count = 0; // convenient counter for all present interactibles

uint8_t level; // all rooms, 256 is overkill but overkill is what's needed
uint8_t fade, fademode; // the variable managing screen fade
// remember that FADE dictates ALPHA, NOT the RGB values
uint8_t fadespeed = 3;

WTH_MainMenu menu; // main menu

uint8_t options[2];

uint8_t input_keys;
uint8_t input_prevkeys;
uint8_t xtrakeys;

void saveGame();
void loadGame();
// frontend, please add details

uint8_t addObject(int nx, int ny, uint8_t objType) {
	if (interacts_count >= INTER_LIMIT) {
		printf("addObject: Tried adding object over the limit!");
		return 0;
	};
	uint8_t index = interacts_count;
	if (objType == INTERTYPE_ARTIFACT && (plr.artifacts & (1 << interacts[index].vars[0]))) return 0; // is it an artifact the player already has? don't fucking bother existing
	for (uint8_t i = 0; i < 7; i++) {
		interacts[index].vars[i] = 0;
	};
	interacts[index].x = nx;
	interacts[index].y = ny;
	interacts[index].objID = objType;
	interacts[index].flags |= INTER_ACTIVE;
	interacts_count++;
	
	switch (objType)
	{
		case 255:
			interacts[index].vars[2] = PLR_IDLEFRAMES;
			break;
	}
	
	/// printf("\nCreated object number %i at (%i, %i) of object type %i", index, interacts[index].x, interacts[index].y, interacts[index].objID);
	return index;
};

static inline void LoadInRoom() {
	for (uint8_t i = 0; i < interacts_count; i++) {
		if ((interacts[i].flags & INTER_ACTIVE)) {
			interacts[i].flags = 0;
		};
	}
	interacts_count = 0;
	switch (level)
	{
		case 0: // TEST
		    addObject(164, GROUNDLEVEL, INTERTYPE_DOOR);
			interacts[0].vars[0] = 1;
			addObject(320, GROUNDLEVEL, INTERTYPE_YOU);
			addObject(300, 100, INTERTYPE_ARTIFACT);
			addObject(400, 100, INTERTYPE_ARTIFACT);
			addObject(500, 100, INTERTYPE_ARTIFACT);
			addObject(600, 100, INTERTYPE_ARTIFACT);
			interacts[3].vars[0] = 1;
			interacts[4].vars[0] = 2;
			interacts[5].vars[0] = 3;
			break;
		case 1: // house
			addObject(650, 0, INTERTYPE_MOVEBLOCK);
			interacts[0].flags ^= 1;
			addObject(182,200,INTERTYPE_COAT);
			addObject(0,218,INTERTYPE_DECOR);
			interacts[2].vars[0] = 10;
			addObject(0, 0, INTERTYPE_MOVEBLOCK);
			addObject(615, GROUNDLEVEL - 12, INTERTYPE_DOOR);
			interacts[4].vars[0] = 2;
			break;
		case 2:
			interacts[addObject(1920, 0, INTERTYPE_TRIGGER)].vars[0] = 3;
			break; // immediate vincinity of outside the house
		case 3: // endless streets
			interacts[ addObject(plr.x, 0, INTERTYPE_MOVETRACK) ].vars[1] = 4;
			break;
		case 4:
			interacts[ addObject(270, 0, INTERTYPE_DOOR) ].vars[0] = 5;
			break; // hospital amidst the streets
		case 5: // hospital main lobby (badge artifact)
			if (plr.artifacts >= 15) interacts[ addObject(-20, 0, INTERTYPE_DOOR) ].vars[0] = 10;
			break;
		/*
		case 6: // hos. hallway to/from patient rooms
		case 7: // hos. patient rooms (knife artifact)
		case 8: // hos. dark room (doughnut artifact)
		case 9: // hos. Richie room (mirror artiface)
		case 10: // blank void
			break;*/
	}
};

static inline void changeRoom(uint8_t input) {
	level = input;
	plr.x = cam.x = 50;
	plr.y = GROUNDLEVEL;
    cam.y = 0;
	plr.hsp = plr.vsp = plr.hsp_sub = plr.vsp_sub = 0;
	if (plr.flags & FLAG_CANTMOVE) plr.flags ^= FLAG_CANTMOVE;
	LoadInRoom();
};

void interact_step(WTH_Interactible *REF) {
	if (!(REF->flags & INTER_ACTIVE)) return;
	switch (REF->objID) {
		case INTERTYPE_TRIGGER:
			if ((REF->flags & FLAG_HASCOAT) && !(plr.flags & FLAG_HASCOAT)) {
				if (plr.x >= REF->x) {
					plr.hsp = 0;
					plr.x = REF->x;
				};
				return;
			};
			if (plr.x >= REF->x) {
				changeRoom(REF->vars[0]);
			};
			break;
		case INTERTYPE_DOOR:
			if (!(REF->flags & 1 << 4)) {
				if (!(plr.flags & FLAG_GROUNDED)) return;
				if (plr.x >= REF->x - 40 && plr.x < REF->x + 40) {
					// printf("Player can enter door");
					signalDraw(DRAW_PLRUPARROW);
					if ((input_keys & KEY_UP)) {
						signalPlaySFX(4);
						plr.flags ^= FLAG_CANTMOVE | PFLAG_HALTANIM;
						plr.animindex = 2;
						plr.animframe = plr.animtimer = 0;
						plr.animflimit = PLR_IDLE1FRAMES;
						plr.hsp = plr.vsp = 0;
						REF->vars[1] = 40;
						REF->flags ^= 1 << 4;
						signalDraw(DRAW_FADEOUT);
					};
				}
			} else {
				if (REF->vars[1] == 0) {
					plr.flags ^= PFLAG_HALTANIM;
					plr.animindex = 0;
					plr.animflimit = PLR_IDLEFRAMES;
					changeRoom(REF->vars[0]);
					signalDraw(DRAW_FADEIN);
					fadespeed = 3;
				} else {
					if (plr.x - 1 < REF->x) plr.x++;
					if (plr.x + 1 > REF->x) plr.x--;
					REF->vars[1]--;
					fadespeed++;
				};
			};
			break;
		case INTERTYPE_COAT:
			if (plr.x >= REF->x - 40 && plr.x < REF->x + 40) {
				// printf("Player can enter door");
				signalDraw(DRAW_PLRUPARROW);
				if (WTH_keyPressed(KEY_UP)) {
					plr.flags ^= FLAG_HASCOAT;
					signalPlaySFX(3);
				};
			};
			break;
		case INTERTYPE_ARTIFACT:
			if (plr.x >= REF->x - 20 && plr.x < REF->x + 20) {
				signalDraw(DRAW_PLRUPARROW);
				if (WTH_keyPressed(KEY_UP)) {
					REF->flags ^= INTER_ACTIVE; // disable it
					plr.artifacts ^= 1 << REF->vars[0];
					signalPlaySFX(6 + REF->vars[0]);
				};
			};
			break;
		case INTERTYPE_MOVETRACK:
			// to-do: find a way to get the POSITIVE ONLY distance
			// between the player and the movetracker's X
			printf("%i", REF->vars[0]);
			if (REF->x != plr.x) REF->vars[0] += plr.x + (REF->x <= plr.x) ? REF->x : -REF->x;
			if (REF->vars[0] > 2500) {
                level = REF->vars[1];
				LoadInRoom();
				while (plr.x > 600) plr.x -= 600;
			};
			REF->x = plr.x;
			break;
		case INTERTYPE_YOU:
			// 0 = animtimer, 1 = animframe, 2 = animflimit
			// printf("FAKE PLAYER");
			REF->vars[0]++;
			if (REF->vars[0] >= 6) {
				REF->vars[1]++;
				REF->vars[0] = 0;
			};
			if (REF->vars[1] > REF->vars[2]) REF->vars[1] = 0;
			break;
		case INTERTYPE_MOVEBLOCK:
			if (plr.x < REF->x && !(REF->flags & 1) || (REF->flags & 1) && plr.x > REF->x) {
				plr.x = REF->x;
				plr.hsp = plr.hsp_sub = 0;
			};
		    break;
	}
};

void manageOptions() {
	switch (menu.menuselect)
	{
		case 0: case 1: case 2: options[0] ^= 1 << menu.menuselect; break; // trust me, this shit works
		case 3: options[0] ^= WTHOPTS_2XSIZE; signalDraw(DRAW_CHECK2XSIZE); break;
		case 4: options[0] ^= WTHOPTS_FULLSC; signalDraw(DRAW_CHECKFULLSC); break;
		case 5: options[0] ^= WTHOPTS_DOSONG; break;
		case 6: saveGame(); menu.menuindex = menu.menuselect = 0; menu.menulimit = 2; break;
	}
};

void start() {
	plr.x = 50;
	plr.y = GROUNDLEVEL;
	plr.state = 1;
	//plr.flags = 0;
	cam.x = cam.y = 0;
	GAME_STATE = 0;
	plr.animflimit = PLR_IDLEFRAMES;
	menu.menuselect = 0;
	loadGame();
	signalDraw(DRAW_CHECK2XSIZE);
	signalDraw(DRAW_CHECKFULLSC);
};

void step() {
	switch (GAME_STATE)
	{
	case 0:
		if (srvdata[0] != 0) break;
		GAME_STATE = 1;
		signalPlayMUS(0);
		signalDraw(DRAW_FADEIN);
		fadespeed = 2;
		changeRoom(0);
		break;
	case 1:
		if (fademode == 1 && fade > 0) {
			if (fade - fadespeed < 0)
				fade = 0;
			else
				fade -= fadespeed;
		};
		if (fademode == 2 && fade < 255) {
			if (fade + fadespeed > 255)
				fade = 255;
			else
				fade += fadespeed;
		};
		switch (plr.state)
		{
			case PSTATE_NORMAL:
				if (!(plr.flags & FLAG_CANTMOVE)) {
					if ((input_keys & KEY_LEFT) && plr.hsp > -3) {
						plr.hsp_sub -= SUBPIXELUNIT_ACCURACY >> 3;
						plr.animindex = 1;
						plr.animflimit = PLR_WALKFRAMES;
						if (plr.hsp > 0) plr.hsp = 0;
						if (!(plr.flags & FLAG_FLIPPED)) plr.flags ^= FLAG_FLIPPED;
					};
					if ((input_keys & KEY_RIGHT) && plr.hsp < 3) {
						plr.hsp_sub += SUBPIXELUNIT_ACCURACY >> 3;
						plr.animindex = 1;
						plr.animflimit = PLR_WALKFRAMES;
						if (plr.hsp < 1) plr.hsp = 0;
						if (plr.flags & FLAG_FLIPPED) plr.flags ^= FLAG_FLIPPED;
					};
					if (plr.jumptimer == 0 && (input_keys & KEY_JUMP) && (plr.flags & FLAG_GROUNDED)) {
						plr.vsp = -3;
						plr.vsp_sub = 0;
						plr.flags ^= FLAG_GROUNDED;
						signalPlaySFX(1);
						plr.jumptimer = 90;
					};
					if (!(input_keys & KEY_LEFT) && !(input_keys & KEY_RIGHT) && (plr.flags & FLAG_GROUNDED)) {
						plr.hsp = 0;
						plr.animindex = 0;
						plr.animflimit = PLR_IDLEFRAMES;
					};
					if (plr.jumptimer > 0) plr.jumptimer--;
				};
				
				if (!(plr.flags & FLAG_NOPHYS)) {
					if (!(plr.flags & FLAG_GROUNDED)) {
						plr.vsp_sub += PRE_GRAVITY;
					}
					if (plr.y > GROUNDLEVEL || plr.y + plr.vsp > GROUNDLEVEL) {
						if (!(plr.flags & FLAG_GROUNDED)) {
							plr.flags |= FLAG_GROUNDED;
						};
						plr.vsp = 0;
						plr.vsp_sub = 0;
						plr.y = GROUNDLEVEL;
					};
					// if the calc messes up at long distances, try changing it to an int
					if (plr.vsp_sub > SUBPIXELUNIT_ACCURACY) {
					    int16_t calc = (plr.vsp_sub / SUBPIXELUNIT_ACCURACY);
						plr.vsp += calc;
						plr.vsp_sub -= SUBPIXELUNIT_ACCURACY * calc;
					};
					if (plr.vsp_sub < 0) {
					    int16_t calc = (plr.vsp_sub / SUBPIXELUNIT_ACCURACY);
						plr.vsp -= calc;
						plr.vsp_sub += SUBPIXELUNIT_ACCURACY * calc;
					};
					
					if (plr.hsp_sub > SUBPIXELUNIT_ACCURACY) {
					    int16_t calc = (plr.hsp_sub / SUBPIXELUNIT_ACCURACY);
						plr.hsp += calc;
						plr.hsp_sub -= SUBPIXELUNIT_ACCURACY * calc;
					};
					if (plr.hsp_sub < 0) {
					    int16_t calc = (plr.hsp_sub / SUBPIXELUNIT_ACCURACY);
						plr.hsp += calc;
						plr.hsp_sub -= SUBPIXELUNIT_ACCURACY * calc; // HOW does this make ANY logical sense to work properly
					};
					plr.x += plr.hsp;
					plr.y += plr.vsp;
				};
				break;
			case PSTATE_SLEEPING: default:
				plr.animindex = 3;
				plr.animflimit = PLR_SLEEPFRAMES;
				if (input_keys & KEY_JUMP && !(input_prevkeys & KEY_JUMP)) {
					plr.state = PSTATE_NORMAL;
					plr.animindex = 1;
					plr.vsp = -3;
					plr.vsp_sub = 0;
					signalPlaySFX(1);
					plr.jumptimer = 90;
					plr.flags = 0;
				};
				break;
		}
			
		if (!(options[0] & WTHOPTS_DEVCAM)) {
			if (plr.x > cam.x + (WIDTH - 64)) {
				cam.x += plr.x - (cam.x + (WIDTH - 64));
			};
			if (plr.x < cam.x + 64) {
				cam.x -= (cam.x + 64) - plr.x;
			};
		} else {
			if (xtrakeys & KEY_RIGHT) cam.x += 3;
			if (xtrakeys & KEY_LEFT) cam.x -= 3;
			if (xtrakeys & KEY_UP) cam.y -= 3;
			if (xtrakeys & KEY_DOWN) cam.y += 3;
		};

		plr.animtimer++;
		if (plr.animtimer >= 6 && !( (plr.flags & PFLAG_HALTANIM) && plr.animframe == plr.animflimit) ) {
			plr.animframe += (plr.flags & PFLAG_REVERSEANIM) ? -1 : 1;
			plr.animtimer = 0;
		};
		if (!(plr.flags & PFLAG_HALTANIM)) {
			if (plr.animframe > plr.animflimit) plr.animframe = 0;
			if (plr.animframe < 0) plr.animframe = plr.animflimit;
		}
		if (plr.flags & FLAG_GROUNDED) {
			if (plr.animframe == 1 && plr.animflimit == PLR_WALKFRAMES && !(plr.flags & FLAG_STEPDEBOUNCE)) {
				signalPlaySFX(0);
				plr.flags += FLAG_STEPDEBOUNCE;
			} else if (plr.animframe != 1 && (plr.flags & FLAG_STEPDEBOUNCE)) plr.flags -= FLAG_STEPDEBOUNCE;
		};
		
		if (WTH_keyPressed(KEY_MENU)) {
			GAME_STATE = 3;
			menu.menuselect = 0;
            signalMisc(MISC_PAUSEMUSIC);
		};
		
		for (uint8_t i = 0; i < INTER_LIMIT; i++) {
			if ((interacts[i].flags & INTER_ACTIVE)) {
				interact_step(&interacts[i]);
			};
		};
		break;
	case 3:
		if (WTH_keyPressed(KEY_UP) && menu.menuselect > 0) {
			menu.menuselect--;
			signalPlaySFX(0);
		};
		if (WTH_keyPressed(KEY_DOWN)) {
			menu.menuselect++;
			signalPlaySFX(0);
		};
		if (WTH_keyPressed(KEY_JUMP)) {
			signalPlaySFX(2);
			if (menu.menuindex == 0) {
				switch (menu.menuselect)
				{
					case 0: GAME_STATE = 1; signalMisc(MISC_RESUMEMUSIC);break;
					//case 1: options[0] ^= WTHOPTS_DEVCAM; break;
					case 1: menu.menuindex = 1; menu.menuselect = 0; break;
					case 2:
					    plr.artifacts = 0;
                        signalMisc(MISC_STOPMUSIC);
	                    plr.flags = 0; // if this causes issues, move it to start()
					    start();
					    break;
				}
			} else manageOptions();
		};
		break;
	}
	
	input_prevkeys = input_keys;
	input_keys = 0;
	if (options[0] & WTHOPTS_DEVCAM) xtrakeys = 0;
};
