// SDL2_net powered server frontend for Wake to Hell with Friends

#include <stdio.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_net.h>

#undef main

TCPsocket NET_serversocket;
TCPsocket clients[8];
SDLNet_SocketSet client_socketset; // for keeping track of client shit I guess

int16_t plrghosts[6][8];
uint8_t srvdata[2];

void step_net() {
	for (uint8_t i = 0; i < 7; i++) {
		if (!plrghosts[i][5]) break;
		SDLNet_TCP_Send(clients[i], &plrghosts, sizeof(plrghosts));
		int16_t plrdata[5];
		SDLNet_TCP_Recv(clients[i], &plrdata, sizeof(plrdata));
		for (uint8_t ii = 0; ii < 5; ii++) {
		    if (!plrghosts[i][5]) continue;
			plrghosts[i][ii] = plrdata[ii];
		};
		SDLNet_TCP_Send(clients[i], &srvdata, 2);
	};
	srvdata[1] = 0;
};

int main() {
	if (SDL_Init(SDL_INIT_EVERYTHING) != 0) return 1;
	if (SDLNet_Init() != 0) return 1;
	
	IPaddress myIP;
	client_socketset = SDLNet_AllocSocketSet(8);
	
	uint8_t running = 1;
	SDL_Event event;
	printf("WAKE to HELL with Friends!\nServer Hosting Program\nA server should be hosted to your local port 5029. Use playit.gg or port forwarding to let people join!\n");
	printf("Opening TCP socket at 5029 and starting server...\n");
	SDLNet_ResolveHost(&myIP, NULL, 5029);
	NET_serversocket = SDLNet_TCP_Open(&myIP);
	
	while (running) {
		uint8_t readyplrs = SDLNet_CheckSockets(client_socketset, 0);
		TCPsocket temp = SDLNet_TCP_Accept(NET_serversocket);
		if (temp) {
			uint8_t joinSignal = 0;
			if (srvdata[0] < 8) {
				SDLNet_TCP_AddSocket(client_socketset, temp);
				//activePlayers ^= 1 << plrcount;
				clients[srvdata[0]] = temp;
				plrghosts[srvdata[0]][5] = 1; // this player is now connected :D
				srvdata[0]++;
				printf("New player connected as client #%i.", srvdata[0] - 1);
			} else {
				printf("Player tried connecting, but the server is full!");
				joinSignal = 1;
			};
			if (readyplrs) {
			    joinSignal = 1;
			    uint8_t plrcount = srvdata[0];
				if (SDLNet_TCP_Send(temp, &srvdata, 2)) { // send over the join signal
					printf("Sent join signal properly!");
					srvdata[1] = 1;
				} else {
					printf("Can't send join signal properly!");
				};
			};
		};
		step_net();
		//SDL_Delay(16.667f);
	}
	
	SDLNet_TCP_Close(NET_serversocket);
	SDLNet_Quit();
	SDL_Quit();
	
	return 0;
};
