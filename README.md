# Wake to Hell with Friends!

A modification for Wake to Hell that tries adding scuffed TCP multiplayer via a client and server connection.

Made with love and hatred by blitzdoughnuts, author of Ludicrital and professional lunatic.

## What the Hell is this?

A mistake.

To be exact, this was made in under half-a-day and adds online connections via TCP that allow players to see eachother via "ghost players" that share their every move, minus the actual action being propogated to all clients.

## Known Bugs, Issues, and Mishaps

All bugs and mishaps from the latest (as of 7/17/2023) version of the game, as well as the following:

- The server doesn't detect players disconnecting, and can't send out server messages properly
- The client and/or server are very prone to crashing at certain points, likely due to how I have zero fucking idea what I am doing
- Players can be seen from all rooms when they should only be visible in the room they're present in on their client
- ALl clients are as slow as their weakest connection; the game uses P2P when I intended for it to use Client-Server

## Licensing

All licenses from the base game apply.
Code modifications made to add online multiplayer are under the CC0. I wouldn't advise using it as learning material, however.